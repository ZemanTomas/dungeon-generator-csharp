using System;
using System.Text;
using System.Collections.Generic;

namespace dungeon_generator{
    class Room : IComparable<Room>{ //properties
        public Room(int _w,int _h,int _x,int _y,int _walls){
            w = _w;
            h = _h;
            x = _x;
            y = _y;
            walls = _walls;
            boss = false;
            hidden = false;
            entrance = false;
            near = new List<Room>();
        }
        public int w,h,x,y; //WxH at X:Y
        public int walls; //Walls
        //Identification for sorting
        public int num; //Room number
        //Graph variables
        public List<Room> near;
        //metadata
        public bool entrance;//Is entrance
        public int depth;//Distance to entrance
        public bool boss;//Is boss room
        public bool hidden;//Is hidden

        public string getMeta(){
            StringBuilder sb = new StringBuilder();
            sb.Append(getID());
            sb.Append(" - ");
            sb.Append(getMetaEntrance());
            sb.Append(getMetaBoss());
            sb.Append(getMetaHidden());
            sb.Append(getMetaDepth());
            sb.Append(getMetaSize());
            sb.Append("\t");
            sb.Append(getMetaNeighbors());
            sb.Append("\n");
            return sb.ToString();
        }

        private string getID(){
            return num.ToString();
        }
        private string getMetaEntrance(){
            return entrance ? "E" : "";
        }
        private string getMetaBoss(){
            return boss ? "B" : "";
        }
        private string getMetaHidden(){
            return hidden ? "H" : "";
        }
        //return list of neighbors
        private string getMetaNeighbors(){
            if(near != null && near.Count > 0){
                StringBuilder sb = new StringBuilder("(");
                sb.Append(near[0].num.ToString());
                for(int i = 1; i < near.Count; i++){
                    sb.Append(",");
                    sb.Append(near[i].num.ToString());
                }
                sb.Append(")");
                return sb.ToString();
            }else{
                return "()";
            }
        }
        private string getMetaDepth(){
            return "("+depth.ToString()+")";
        }
        //return size (x,y,w,h) of room without walls
        private string getMetaSize(){
            return $"(({x+walls},{y+walls}:{x+w-walls-1},{y+h-walls-1}))";
        }
        //comparing rooms
        public int CompareTo(Room r){
            if(r == null){
                return 1;
            }
            if(y+walls < r.y+r.walls){return -1;}
            if(y+walls > r.y+r.walls){return 1;}
            if(x+walls < r.x+r.walls){return -1;}
            if(x+walls > r.x+r.walls){return 1;}
            return 0;
        }
        //contains x:y
        public bool Contains(int _x, int _y){
            return (_x >= x+walls && _x < x+w-walls) && (_y >= y+walls && _y < y+h-walls);
        }
    }
}
