using System;
using System.Text;
using System.IO;
using System.Linq;

namespace dungeon_generator{
    class Dungeon{
        public Dungeon(Cell[,] _cells, Room[] _rooms, int _seed, Room _entrance = null){
            cells = _cells;
            rooms = _rooms;
            seed = _seed;
            //if entrance null, check for it
            if(_entrance == null && rooms.Length > 0){
                entrance = rooms[0];
                foreach(Room room in rooms){
                    if(room.entrance){
                        entrance = room;
                    }
                }
            }else{
                entrance = _entrance;
            }
        }
        public override string ToString(){
            if(cells == null){
                return "Empty Dungeon";
            }else{
                return getDungeon() + getMeta();
            }
        }
        public string getDungeon(){
            StringBuilder sb = new StringBuilder("");
            for(int i = 0; i < cells.GetLength(0); i++){
                for(int j = 0; j < cells.GetLength(1); j++){
                    sb.Append(cells[i,j].ToString());
                }
                sb.Append("\n");
            }
            return sb.ToString();
        }
        public string getMeta(){
            StringBuilder sb = new StringBuilder("");
            foreach(Room room in rooms){
                sb.Append(room.getMeta());
            }
            sb.Append("Seed: "+seed.ToString()+"\n");
            return sb.ToString();
        }
        public int getWidth(){
            return cells.GetLength(1);
        }
        public int getHeight(){
            return cells.GetLength(0);
        }
        public void setEntrance(int roomNum){
            if(roomNum < rooms.Length){
                entrance.entrance = false;
                entrance = rooms[roomNum];
                entrance.entrance = true;
            }
        }

        Room[] rooms;
        Room entrance;
        Cell[,] cells;
        //metadata
        int seed;
    }
}
