using System;
using System.Text;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace dungeon_generator{

    class SameTuplesComparer<T1, T2> : EqualityComparer<Tuple<T1, T2>>{
        public override bool Equals(Tuple<T1, T2> t1, Tuple<T1, T2> t2){
            return t1.Item1.Equals(t2.Item1) && t1.Item2.Equals(t2.Item2);
        }

        public override int GetHashCode(Tuple<T1, T2> t){
            return base.GetHashCode();
        }
    }

    class DungeonGenerator{
        public static Dungeon Generate(int width, int height, int seed, int neighborDistance = 3, int edgeChance = 5){
            //Check for seed, create a random number generator
            Random rand;
            int realSeed = seed;
            if(seed >= 0){
                rand = new Random(seed);
            }else{
                Random temp = new Random();
                realSeed = temp.Next();
                rand = new Random(realSeed);
            }
            Cell[,] cells = new Cell[height,width];
            for(int i = 0; i < height; i++){
                for(int j = 0; j < width; j++){
                    cells[i,j] = new Cell('W');
                }
            }
            Room[] rooms;
            List<Room> _rooms;

            //Generate Rooms
            GenerateRooms(ref cells, rand, width, height, 0, 0, 1, 0, out _rooms);
            rooms = _rooms.ToArray();

            //Sort rooms and number them
            SortAndNumberRooms(ref rooms);

            //Generate graph
            GenerateGraph(ref cells, ref rooms, rand, width, height, neighborDistance);

            //Find spanning tree with few random edges
            FindSpanningTreePlus(ref rooms, rand, edgeChance);

            //Generate passages
            GeneratePassages(ref cells, ref rooms, rand);

            //Fill rooms with metadata
            AddMeta(ref rooms, rand);

            return new Dungeon(cells, rooms, realSeed);
        }
        public static void RegenerateMetadata(ref Dungeon dungeon){//WIP
            //Check rooms
            //Fill rooms
        }
        //Checks if a string is a valid dungeon
        public static bool CheckValidity(int width, int height, string dungeon){
            //Split on newlines
            string[] lines = dungeon.Split("\n");
            if(lines.Length < height){
                return false;
            }
            //Check if each character is valid
            for(int i = 0; i < height; i++){
                for(int j = 0; j < width; j++){
                    char c = lines[i][j];
                    if(!Cell.charIsValid(c)){
                        return false;
                    }
                }
            }
            return true;
        }
        //Parse valid string into cell array
        public static Cell[,] ParseCells(int width, int height, string dungeon){
            string[] lines = dungeon.Split("\n");
            Cell[,] cells = new Cell[height,width];
            for(int i = 0; i < height; i++){
                for(int j = 0; j < width; j++){
                    cells[i,j] = new Cell(lines[i][j]);
                }
            }
            return cells;
        }
        
        //Generates rooms recursively
        private static void GenerateRooms(ref Cell[,] cells, Random rand, int width, int height, int x, int y, int walls_up, int depth, out List<Room> rooms){
            //variables
            rooms = new List<Room>();
            List<Room> _roomsr = new List<Room>();
            List<Room> _roomsl = new List<Room>();
            bool axis = (depth % 2) == 0; //0 - x, 1 - y
            int rnd_wall,rnd_split,walls;
            //Generate walls number
            walls = 1;//rand.Next(1,1+width/6);
            //debug
            if(Globals.DEBUG){
                for(int i = 0; i < depth; i++){Console.Write(" ");}
                Console.WriteLine("X:"+x.ToString()+",Y:"+y.ToString()+" W:"+width.ToString()+" H:"+height.ToString()+" W:"+walls.ToString());
            }
            //Stop condition
            bool stop = false;
            if(axis){
                if(width-walls*2 <= 6){stop = true;}
                if(height <= 5){stop = true;}
            }else{
                if(width <= 5){stop = true;}
                if(height-walls*2 <= 6){stop = true;}
            }
            if(rand.Next(100) < Math.Min(25,3*depth)){stop = true;}
            //Stop and create a new room and update cells
            if(stop){
                //Check if positive dimensions
                if(width-2*walls <= 0 || height-2*walls <= 0){
                    return;
                }
                rooms.Add(new Room(width,height,x,y,walls));
                for(int i = x+walls; i < x+width-walls; i++){
                    for(int j = y+walls; j < y+height-walls; j++){
                        cells[j,i] = new Cell('R');
                    }
                }
                return;
            }
            //Generate split number
            if(axis){
                rnd_split = (width/4) + rand.Next(width/2);
            }else{
                rnd_split = (height/4) + rand.Next(height/2);
            }
            //Split/recurse
            if(axis){
                //Left
                GenerateRooms(ref cells, rand, rnd_split, height, x, y, walls, depth+1, out _roomsl);
                //Right
                GenerateRooms(ref cells, rand, width-rnd_split, height, x+rnd_split, y, walls, depth+1, out _roomsr);
            }else{
                //Up
                GenerateRooms(ref cells, rand, width, rnd_split, x, y, walls, depth+1, out _roomsl);
                //Down
                GenerateRooms(ref cells, rand, width, height-rnd_split, x, y+rnd_split, walls, depth+1, out _roomsr);
            }

            rooms = _roomsr.Concat(_roomsl).ToList();
        }
        //Sorts and numbers rooms according to position from top-left to bottom-right and marks first room as entrance
        private static void SortAndNumberRooms(ref Room[] rooms){
            if(rooms.Length == 0){return;}
            Array.Sort(rooms);
            rooms[0].entrance = true;
            for(int i = 0; i < rooms.Length; i++){
                rooms[i].num = i;
            }
        }
        //Generate complete graph
        private static void GenerateGraph(ref Cell[,] cells, ref Room[] rooms, Random rand, int width, int height, int neighborDistance){
            //Check right/bottom max distance c = 4 (O(h*w))
            for(int i = 0; i < rooms.Length; ++i){
                Room r = rooms[i];
                //Temp set for found neighbors
                HashSet<Room> neighbors = new HashSet<Room>(r.near);
                //Check right side
                for(int y = r.y+r.walls; y < r.y+r.h-r.walls; ++y){
                    if(y>=height){break;}
                    for(int x = r.x+r.w; x < r.x+r.w+neighborDistance; ++x){
                        if(x>=width){break;}
                        if(cells[y,x].isType(CellType.Room)){
                            //Find room at x,y
                            Room f = FindRoomAt(ref rooms,x,y);
                            if(f != null && neighbors.Add(f)){
                                f.near.Add(rooms[i]);
                            }
                            break;
                        }
                    }
                }
                //Check bottom side
                for(int x = r.x+r.walls; x < r.x+r.w-r.walls; ++x){
                    if(x>=width){break;}
                    for(int y = r.y+r.h; y < r.y+r.h+neighborDistance; ++y){
                        if(y>=height){break;}
                        if(cells[y,x].isType(CellType.Room)){
                            //Find room at x,y
                            Room f = FindRoomAt(ref rooms,x,y);
                            if(f != null && neighbors.Add(f)){
                                f.near.Add(rooms[i]);
                            }
                            break;
                        }
                    }
                }
                //Set set to r
                rooms[i].near = neighbors.ToList();
            }
            //Sort each rooms list of near rooms
            for(int i = 0; i < rooms.Length; ++i){
                rooms[i].near.Sort();
            }
        }
        //Find spanning tree with few random edges
        private static void FindSpanningTreePlus(ref Room[] rooms, Random rand, int chance = 6){
            //Get edges
            HashSet<Tuple<Room,Room>> dfs = GetRandomDFS(ref rooms, rand);
            //For each edge try to add to hashset
            foreach(Room room in rooms){
                foreach(Room r in room.near){
                    //Each edge is checked twice, so the chance per edge needs to be adjusted accordingly so the overall chance is 1/chance
                    if(rand.Next(0,chance*2-1) == 0){
                        dfs.Add(new Tuple<Room,Room>(room,r));
                        dfs.Add(new Tuple<Room,Room>(r,room));
                    }
                }
            }
            //Replace edges in rooms
            for(int i = 0; i < rooms.Length; ++i){
                rooms[i].near.Clear();
            }
            //Need to add only one direction per tuple since both directions are in set already
            foreach(Tuple<Room,Room> t in dfs){
                t.Item1.near.Add(t.Item2);
            }
        }
        //Gets a spanning tree from a random room
        private static HashSet<Tuple<Room,Room>> GetRandomDFS(ref Room[] rooms, Random rand){
            HashSet<Tuple<Room,Room>> ret = new HashSet<Tuple<Room,Room>>();
            List<Room> to_visit = new List<Room>();to_visit.Add(rooms[rand.Next(0,rooms.Length-1)]);
            HashSet<Room> visited = new HashSet<Room>();visited.Add(to_visit[0]);

            while(visited.Count < rooms.Length){
                //Check if all nodes visited, if not, add a not-visited room to to_visit
                if(visited.Count > 1){
                    foreach(Room r in rooms){
                        if(!visited.Contains(r)){
                            to_visit.Add(r);
                            visited.Add(r);
                            break;
                        }
                    }
                }
                while(to_visit.Count != 0){
                    Room current = to_visit[0];
                    to_visit.Remove(current);

                    foreach(Room r in current.near){
                        if(!visited.Contains(r)){
                            ret.Add(new Tuple<Room,Room>(r,current));
                            ret.Add(new Tuple<Room,Room>(current,r));
                            to_visit.Add(r);
                            visited.Add(r);
                        }
                    }
                }
            }

            return ret;
        }
        //Generate passages in cells
        private static void GeneratePassages(ref Cell[,] cells, ref Room[] rooms, Random rand){
            //Created
            HashSet<Tuple<Room,Room>> created = new HashSet<Tuple<Room,Room>>(rooms.Length, new SameTuplesComparer<Room,Room>());
            //Foreach
            foreach(Room r1 in rooms){
                foreach(Room r2 in r1.near){
                    Room tl,br;
                    if(r1.CompareTo(r2) < 0){
                        tl = r1;
                        br = r2;
                    }else{
                        tl = r2;
                        br = r1;
                    }
                    if(tl.x > br.x+br.w-1){
                        Room temp = tl;
                        tl = br;
                        br = temp;
                    }
                    //Skip if created
                    if(created.Contains(new Tuple<Room,Room>(tl,br))){
                        continue;
                    }
                    //List of possible passages
                    List<int> possible = new List<int>();
                    bool right = tl.y+tl.h-1>br.y;
                    if(right){//right
                    //Max/min height
                    int low = Math.Max(tl.y+tl.walls,br.y+br.walls);
                    int high = Math.Min(tl.y+tl.h-tl.walls,br.y+br.h-br.walls);

                    for(;low < high;low++){
                        bool clear = true;
                        for(int i = tl.x+tl.w-tl.walls;i < br.x+br.walls;i++){
                            if(FindRoomAt(ref rooms,i,low) != null){
                                clear = false;
                                break;
                            }
                        }
                        if(clear){
                            possible.Add(low);
                        }
                    }

                    }else{//down
                    //Max/min width
                    int low = Math.Max(tl.x+tl.walls,br.x+br.walls);
                    int high = Math.Min(tl.x+tl.w-tl.walls,br.x+br.w-br.walls);

                    for(;low < high;low++){
                        bool clear = true;
                        for(int i = tl.y+tl.h-tl.walls;i < br.y+br.walls;i++){
                            if(FindRoomAt(ref rooms,low,i) != null){
                                clear = false;
                                break;
                            }
                        }
                        if(clear){
                            possible.Add(low);
                        }
                    }

                    }
                    //Select one way in random
                    if(possible.Count == 0){//No possible passages are an error
                        //Error
                        continue;
                    }
                    int passage = possible[rand.Next(0,possible.Count-1)];
                    if(right){
                        for(int i = tl.x+tl.w-tl.walls;i < br.x+br.walls;i++){
                            cells[passage,i] = new Cell('P');
                        }
                    }else{
                        for(int i = tl.y+tl.h-tl.walls;i < br.y+br.walls;i++){
                            cells[i,passage] = new Cell('P');
                        }
                    }

                    //Mark as created
                    created.Add(new Tuple<Room,Room>(tl,br));

                }
            }
        }
        //Add metadata
        private static void AddMeta(ref Room[] rooms, Random rand){
            //Boss room/distance - bfs
            HashSet<Room> visited = new HashSet<Room>();visited.Add(rooms[0]);
            List<Room> to_visit = new List<Room>();to_visit.Add(rooms[0]);
            int distance = 0, layer = 1, nextLayer = 0; //current distance and number of nodes in a layer
            while(to_visit.Count != 0){
                Room current = to_visit[0];
                to_visit.Remove(current);
                current.depth = distance;
                foreach(Room n in current.near){
                    if(!visited.Contains(n)){
                        to_visit.Add(n);
                        visited.Add(n);
                        nextLayer++;
                    }
                }
                //Switch to another layer
                if(--layer == 0){
                    distance++;
                    layer = nextLayer;
                    nextLayer = 0;
                }
            }
            
            //Select a boss room out of rooms with max depth
            int maxDepth = distance - 1;
            List<Room> bossCandidates = new List<Room>();
            foreach(Room r in rooms){
                if(r.depth == maxDepth){
                    bossCandidates.Add(r);
                }
            }
            Room bossRoom = bossCandidates[rand.Next(0,bossCandidates.Count-1)];
            rooms[bossRoom.num].boss = true;
            

            //Hidden room
            //Difficulty - distance
        }

        //Find room at x:y
        private static Room FindRoomAt(ref Room[] rooms, int x, int y){
            foreach(Room r in rooms){
                if(r.Contains(x,y)){return r;}
            }
            return null;
        }
    }
}
