# USER'S GUIDE
This program generates a 2D dungeon. Dungeon is generally a planar graph (nodes and edges connecting them) optionally with metadata describing the graph or its nodes. Dungeons are used in games, mostly in rpg games, where players can explore rooms (nodes) and travel from one room to another (edges). It can be difficult and time consuming to create a dungeon layout by hand. Algorithmical generation is very fast, but doesn't produce as creative results as human brain. But in many cases, this doesn't matter or can be fixed by slightly adjusting the output.

This generator generates a dungeon of a given width and height

## Building

Project file: dungeon-generator.csproj

.NET version >=3.1.8 (lower versions might also work, untested)

**To build:**

`dotnet build`

**To run:**

```
dungeon-generator -w <width> -h <height> [-s <seed>] [-o <filename>] [-d debug]
-w      width, default is 16
-h      height, default is 16
-s      seed >= 0
-d      debug, turned on if anything
-o      filename, otherwise prints to console
```


Output is divided in 3 parts
- WxH matrix of tiles
    - 'W' - Wall
    - '.' - Space(Room)
    - 'P' - Passage
- List of rooms with metadata (E - entrance, B - boss room, H - hidden)
    - <room_num> - \[E\]\[H\]\[B\](<dist_to_entrance>)((<top_left>):(<bottom_right>))\t(<list_of_neighbours>)
    - Rooms are numbered from top left corner to bottom left, traversing dungeon by lines
- Seed


Example output:
<span style="font-family:Monospace; font-size:4em;">
```
WWWWWWWWWWWWWWWW  
W.........PP...W  
W.........WW...W  
W.........WW...W  
WPWWPWWWWWWW...W  
WPWWPWWWWWWW...W  
W.PP......WW...W  
W.WW......WW...W  
W.WWWWWWWWWW...W  
W.WWWWWWWWWW...W  
W.PP......WW...W  
W.WW......WW...W  
W.WW......PP...W  
W.WW......WW...W  
W.WW......WW...W  
WWWWWWWWWWWWWWWW  
0 - E(0)((1,1):(9,3))   (2,1,3)  
1 - (1)((12,1):(14,14)) (0,4)  
2 - (1)((1,6):(1,14))   (0,3,4)  
3 - (1)((4,6):(9,7))    (2,0)  
4 - B(2)((4,10):(9,14)) (2,1)  
Seed: 888719449  
```
</span>

# PROGRAMMER'S GUIDE

File structure:
- Program.cs - Entrypoint, parses arguments, calls DungeonGenerator
- Room.cs - Room definition
- Cell.cs - Cell definition
- Dungeon.cs - Dungeon definition
- Getopt.cs - Parameter parsing
- DungeonGenerator.cs - Dungeon generation 

Dungeon generation:
- Parameters: Seed, Width, Height, [maximumNeighborDistance, chanceOfAddingEdgeToSpanningTree]
- Room Generation
    - Recursive
    - Binary Division
    - Random Stopping + Minimum Size
- Sorting Rooms, Numbering Rooms
- Graph Generation
    - Complete Graph
    - Rooms are only connected if close enough (maximum neighbor distance)
    - Right and bottom sides of all rooms are checked
    - Hash set used for deduplication
    - Sorting of lists of neighbors
- Find spanning tree plus random edges (chance of adding edge to spanning tree)
    - DFS from random room
    - Randomly add edges from complete graph to spanning tree
    - Update Rooms
- Generate Passages
    - For each pair of rooms get list of possible passage locations (only direct lines)
    - Scans space between rooms to right and down
- Add Metadata
    - BFS for distance to entrance
    - Boss room has max depth
