using System;

namespace dungeon_generator{
    enum CellType : byte{
        Wall,
        Room,
        Passage,
        Invalid
    }
    struct Cell{
        CellType type;
        public Cell(char s){
            switch(s){
                case 'W':
                    type = CellType.Wall;
                    break;
                case 'R':
                    type = CellType.Room;
                    break;
                case '.':
                    type = CellType.Room;
                    break;
                case 'P':
                    type = CellType.Passage;
                    break;
                default:
                    type = CellType.Invalid;
                    break;
            }
        }
        public Cell(CellType t){
            type = t;
        }
        public override string ToString(){
            switch(type){
                case CellType.Wall:
                    return "W";
                    break;
                case CellType.Room:
                    return ".";
                    break;
                case CellType.Passage:
                    return "P";
                    break;
                default:
                    return "X";
                    break;
            }
        }
        public bool isType(CellType t){
            return type == t;
        }

        public static bool charIsValid(char c){
            return c == 'W' || c == '.' || c == 'P' || c =='X';
        }
    }
}
