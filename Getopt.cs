using System;
using System.Text;
using System.Collections.Generic;

namespace dungeon_generator{
    //Getopt Class
    //For parsing arguments, only short ones for now
    class Getopt{
        public Getopt(string[] _args, string _usage = ""){
            args = _args;
            usage = _usage;
            MapString = new Dictionary<char,string>();
            MapInteger = new Dictionary<char,int>();
            MapFilename = new Dictionary<char,string>();
            Types = new Dictionary<char,byte>();
            Helps = new Dictionary<char,string>();
        }
        public void AddParamString(char p,string help = "", string? def = null){
            if(Types.ContainsKey(p)){return;}
            Types.Add(p,0);
            Helps.Add(p,help);
            if(def is string val){
                MapString[p] = val;
            }
        }
        public void AddParamInteger(char p,string help = "", int? def = null){
            if(Types.ContainsKey(p)){return;}
            Types.Add(p,1);
            Helps.Add(p,help);
            if(def is int val){
                MapInteger[p] = val;
            }
        }
        public void AddParamFilename(char p,string help = "", string? def = null){
            if(Types.ContainsKey(p)){return;}
            Types.Add(p,2);
            Helps.Add(p,help);
            if(def is string val){
                MapFilename[p] = val;
            }
        }
        public bool Eval(){
            for(int i = 0; i < args.Length; i++){
                if(args[i][0] == '-' && args[i].Length == 2 && i+1 < args.Length){
                    bool success = false;
                    switch(Types[args[i][1]]){
                        case 0:
                            success = ParseString(args[i][1],args[i+1]);
                            break;
                        case 1:
                            success = ParseInteger(args[i][1],args[i+1]);
                            break;
                        case 2:
                            success = ParseFilename(args[i][1],args[i+1]);
                            break;
                    }
                    if(success){
                        i++;
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }
            return true;
        }
        private bool ParseString(char p, string str){
            MapString[p] = str;
            return true;
        }
        private bool ParseInteger(char p, string str){
            int result;
            bool success = Int32.TryParse(str,out result);
            if(success){
                MapInteger[p] = result;
            }
            return success;
        }
        private bool ParseFilename(char p, string str){
            //Is it really a filename?
            MapString[p] = str;
            return true;
        }
        public string getHelp(){
            StringBuilder sb = new StringBuilder(usage);
            sb.Append("\n");
            foreach(var pair in Helps){
                sb.Append(pair.Value);
                sb.Append("\n");
            }
            return sb.ToString();
        }
        public string? getString(char p){
            if(MapString.ContainsKey(p)){
                return MapString[p];
            }else{
                return null;
            }
        }
        public int? getInteger(char p){
            if(MapInteger.ContainsKey(p)){
                return MapInteger[p];
            }else{
                return null;
            }
        }
        public string? getFilename(char p){
            if(MapFilename.ContainsKey(p)){
                return MapFilename[p];
            }else{
                return null;
            }
        }
        string[] args;
        string usage;
        //0 - string
        //1 - integer
        //2 - filename
        Dictionary<char,byte> Types;
        Dictionary<char,string> Helps;
        Dictionary<char,string> MapString;
        Dictionary<char,int> MapInteger;
        Dictionary<char,string> MapFilename;
    }
}
