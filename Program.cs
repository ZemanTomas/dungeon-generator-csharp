﻿using System;
using System.Text;
using System.IO;
using System.Linq;
//TODO:
//filename validity
//comments
//validity check
//maps (htables)
//splicing dungeons
//Multiple algorithms
//ternary expressions
//2D tree for quick cell to room for generateGraph
//debugging
//difficulty per room
//Hidden rooms


//Output format:
//2D cell array
//Sorted list of rooms(room number - metadata(depth)((top left corner)(bottom right corner))\t(list of connected rooms))
//Seed

//Metadata: E - Entrance, B - Boss room, H - Hidden room

namespace dungeon_generator{
    public static class Globals{
        public static bool DEBUG = false;
    }
    class Program{
        static void Main(string[] args){
            //Parse args
            Getopt parser = new Getopt(args,"dungeon_generator -w <width> -h <height> [-s <seed>] [-o <filename>] [-d debug]");
            parser.AddParamInteger('w',"-w\twidth, default is 16",16);
            parser.AddParamInteger('h',"-h\theight, default is 16",16);
            parser.AddParamInteger('s',"-s\tseed >= 0",-1);
            parser.AddParamString('d',"-d\tdebug, turned on if anything");
            parser.AddParamFilename('o',"-o\tfilename, otherwise prints to console");
            bool fail = !parser.Eval();
            int? width = parser.getInteger('w');
            int? height = parser.getInteger('h');
            int? seed = parser.getInteger('s');
            string? filename = parser.getFilename('o');
            string? debug = parser.getString('d');
            if(width == null || height == null || seed == null){fail = true;}
            //Print help if failed parsing args
            if(fail){
                Console.Write(parser.getHelp());
                return;
            }

            //Set debug
            Globals.DEBUG = debug != null;
            //Generate dungeon
            Dungeon d = DungeonGenerator.Generate(width.Value,height.Value,seed.Value);
            //Save or print
            if(filename == null){
                PrintToConsole(d);
            }else{
                SaveToFile(d,filename);
            }
        }

        static void SaveToFile(Dungeon dungeon,string filename){
            using (StreamWriter writer = new StreamWriter(filename)){
                writer.Write(dungeon);
            }
        }
        static void PrintToConsole(Dungeon dungeon){
            Console.Write(dungeon);
        }
    }
}
